import 'package:flutter/material.dart';
import 'consultation.dart';
import 'appbar.dart';
import 'navdrawer.dart';
import 'carousel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: appbar(),
        drawer: DrawerWidget(),
        body: AppBody(),
      ),
    );
  }
}

class AppBody extends StatefulWidget {
  const AppBody({Key? key}) : super(key: key);

  @override
  _AppBodyState createState() => _AppBodyState();
}

class _AppBodyState extends State<AppBody> {
  int _selectedIndex = 0;
  static final List<Widget> _widgetOptions = <Widget>[
    /* Bisa buat widget masing-masing dan replace widget Container() dengan
    widget kalian agar bisa diakses oleh bottom navbar.
    Contoh pada index 0 dan 4 */
    // Index 0 Artikel
    homecarousel(),
    // TODO Index 1 Donasi
    Container(
      margin: EdgeInsets.only(top: 30),
    ),
    // TODO Index 2 Quiz
    Container(
      margin: EdgeInsets.only(top: 30),
    ),
    // TODO Index 3 Thread
    Container(
      margin: EdgeInsets.only(top: 30),
    ),
    // Index 4 Consultation
    const FormulirPendaftaran()
  ];

  void _onTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icon/home.png")),
            label: 'Artikel',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icon/heart.png")),
            label: 'Donasi',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icon/test.png")),
            label: 'Quiz',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icon/chat.png")),
            label: 'Thread',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icon/bipolar.png")),
            label: 'Consultation',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent[700],
        unselectedItemColor: Colors.grey,
        onTap: _onTapped,
      ),
    );
  }
}
