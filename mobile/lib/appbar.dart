import 'package:flutter/material.dart';

class appbar extends StatelessWidget with PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Image.asset(
        'assets/ruangsinggah.png',
        width: 150,
      ),
      centerTitle: true,
      backgroundColor: Color(0xff304ffe),
    );
  }
}
